<?php require_once 'template/header.php'; ?>
<h2>Register</h2>
<form action="user.php" method="post">
    <p>
        <label for="username">User Name:</label>
        <input type="text" name="username" id="username">
    </p>
    <p>
        <label for="email">Email:</label>
        <input type="email" name="email" id="email">
    </p>
    <p>
        <label for="email">Password:</label>
        <input type="password" name="password" id="password">
        <input type="password" name="passwordConfirm" id="passwordConfirm">
    </p>
    <p>
        <button type="submit">Join Now</button>
    </p>
    <input id="message" name="message" type="hidden" value="Welcome to whato.do!">
</form>
<?php require_once 'template/footer.php'; ?>
