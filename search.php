<!DOCTYPE html>
<html ng-app="whatoApp">
<head>
    <!-- Viewport Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- AngularJS  -->
    <script src="./node_modules/angular/angular.js"></script>
    <script src="./node_modules/angular-animate/angular-animate.js"></script>
    <script src="./node_modules/angular-ui-router/release/angular-ui-router.js"></script>
    <!-- Application -->
    <title>whato.do</title>
    <link rel="stylesheet" href="css/app.css">
    <script src="./js/whatoApp.js"></script>
</head>
<body>
    <!-- Application branded header -->
    <header>
        <span>whato</span>.<span>do</span>
        <small>&trade;</small>
        <p>(what to do)</p>
    </header>
    <!-- Application main content area -->
    <main ui-view>
    <input class="form-control" type="text" ng-model="search" placeholder="Search" >

    <button ng-if="!dirList.toggle" class="btn btn-success btn-block" ng-click="dirList.toggle = true"> Show Names </button>
    <button ng-if="dirList.toggle" class="btn btn-danger btn-block" ng-click="dirList.toggle = false"> Hide Names </button>

    <ul ng-if="dirList.toggle">
        <li ng-repeat="item in dirList.list | filter:search" 
            ng-class="item.age>34 ? 'over-35' : 'under-35'"
            ng-include="'person.html'">
        </li>
    </ul>

    <form ng-submit="dirList.addPerson()">
        <p>Add another Person</p>
        <input class="form-control" type="text" placeholder="Name" ng-model="dirList.name">
        <input class="form-control" type="number" placeholder="Age" ng-model="dirList.age">
        <input type="submit" value="Add" class="btn btn-primary btn-block">
    </form>
    </main>
    <!-- Application main navigation -->
    <nav>
        <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">Item 2</a></li>
            <li><a href="#">Item 3</a></li>
            <li><a href="#">Item 4</a></li>
            <li><a href="#">Item 5</a></li>
        </ul>
    </nav>
    <!-- Application footer -->
    <footer>
        <p>&copy; 2016 <a href="#">whato.do</a>.
    </footer>
</body>

</html>
