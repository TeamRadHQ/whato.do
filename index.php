<?php require_once 'template/header.php'; ?>
        <h1>Some Page</h1>
        <h2>Application Sections</h2>
        <h3>Main Navigation</h3>
            <ul>
                <li>Login / Logout</li>
                <li>Signup</li>
                <li>Blog</li>
                <li>User Profile</li>
                <li>Company Information</li>
                <li>User Settings</li>
                <li>Add Listing</li>
                <li>Application Support</li>
                <li>Business Information</li>
                <li>Public Listing</li>
                <li>User Area</li>
            </ul>
        <h3>Content Area</h3>
            <ul>
                <li>User Area</li>
                <li>Business Information</li>
                <li>Company Information</li>
                <li>Add Listing</li>
                <li>Blog</li>
                <li>Public Listing</li>
                <li>User Profile</li>
                <li>User Settings</li>
                <li>Application Support</li>
                <li>Login / Logout</li>
                <li>Signup</li>
            </ul>
        <h3>Application Controls</h3>
            <ul>
                <li>Application Support</li>
                <li>Public Listing</li>
                <li>Add Listing</li>
                <li>User Settings</li>
                <li>Business Information</li>
                <li>User Profile</li>
                <li>Blog</li>
                <li>Company Information</li>
                <li>User Area</li>
                <li>Login / Logout</li>
                <li>Signup</li>
            </ul>
<?php require_once 'template/footer.php'; ?>
