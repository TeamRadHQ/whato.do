<!DOCTYPE html>
<html ng-app="whatoApp">
<head>
    <!-- Viewport Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Application -->
    <title>whato.do</title>
    <link rel="stylesheet" href="css/app.css">
    <script src="./node_modules/jquery/dist/jquery.min.js"></script>
    <script src="./js/whatoApp.js"></script>
</head>
<body>
    <!-- Application branded header -->
    <a class="header" href="/">
        <header>
            <span>whato</span>.<span>do</span>
            <small>&trade;</small>
            <p>(what to do)</p>
        </header>
    </a>
    <!-- Application main content area -->
    <main>