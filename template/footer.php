    </main>
    <!-- Application main navigation -->
    <nav>
        <ul>
            <li class="about"><a href="#">About</a></li>
            <li class="listing"><a href="#">Listing</a></li>
            <li class="blog"><a href="#">Blog</a></li>
<?php
$loggedIn = ['user', 'add', 'single'];
$page = str_replace('/','',$_SERVER['REQUEST_URI']);
$page = str_replace('.php','',$page);
if (in_array($page, $loggedIn)) {
?>
            <li class="login"><a href="user.php">User Area</a>
                <ul class="popUp">
                    <li><a href="listing.php?new=1">Add Listing</a></li>
                    <li><a href="index.php">Logout</a></li>
                </ul>
            </li>
<?php
} else {
?>
            <li class="login"><a href="login.php">Login</a>
                <ul class="popUp">
                    <li><a href="register.php">Register</a></li>
                </ul>
            </li>
<?php
}
?>
        </ul>
    </nav>
    <!-- Application footer -->
    <footer>
        <p>&copy; 2016 <a href="#">whato.do</a>.
    </footer>
</body>

</html>