<?php require_once 'template/header.php'; ?>
<!-- <h1>Settings</h1> -->

<?php
extract($_POST);
if( isset($message) ) {
    echo '<section class="message"><p>'.$message.'</section>';
}
?>

<section class="content profile">
    <button class="settings"><span class="glyphicon glyphicon-pencil"></span></button>
    <h3>Your Profile</h3>
    <table>
    <tr>
        <td>Name</td>
        <td class="name">Billy Bob</td>
    </tr>
    <tr>
        <td>Email</td>
        <td class="email">billy@bob.com</td>
    </tr>
    <tr>
        <td>Company</td>
        <td class="company">Ballers Inc.</td>
    </tr>
    <tr>
        <td>Location</td>
        <td class="location">Footscray, Victoria</td>
    </tr>
    </table>
</section>
<section class="content listings">
    <button class="settings"><span class="glyphicon glyphicon-pencil"></span></button>
    <h3>Your Listings</h3>
    <table>
        <tr>
            <td class="title">RPi Workgroup</td>
            <td class="description"> Monthly workshops dedicated to Raspberry Pi projects. </td>
        </tr>
        <tr>
            <td class="title">PHP Meetup</td>
            <td class="description"> Weekly get together for PHP nerds. </td>
        </tr>
        <tr>
            <td class="title">D&amp;D Meetup </td>
            <td class="description"> Come adventure and roll for some serious DPS! </td>
        </tr>
    </table>
</section>
<section class="content options">
    <button class="settings"><span class="glyphicon glyphicon-pencil"></span></button>
    <h3>Your Settings</h3>
    <table>
        <tr>
            <td>Subscription</td>
            <td class="subscriptions">Basic</td>
        </tr>
        <tr>
            <td>Visibility</td>
            <td class="visibility">Public</td>
        </tr>
        <tr>
            <td>Alerts</td>
            <td class="alerts">Email Only</td>
        </tr>
    </table>
</section>

<?php require_once 'template/footer.php'; ?>
