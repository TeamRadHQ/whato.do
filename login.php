<?php require_once 'template/header.php'; ?>
<h2>Login</h2>
<form action="user.php" method="post">
    <p>
        <label for="username">User / Email:</label>
        <input type="text" name="username" id="username">
    </p>
    <p>
        <label for="email">Password:</label>
        <input type="password" name="password" id="password">
    </p>
    <p>
        <button type="submit">Login</button>
    </p>
    <input id="message" name="message" type="hidden" value="Welcome back Billy Bob!">
</form>
<?php require_once 'template/footer.php'; ?>
