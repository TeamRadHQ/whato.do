var people = [
	{
		"id" : 		1,
		"name" : 	"Dougy Smith"
	},
	{
		"id" : 		2,
		"name" : 	"Jill Jones"
	},
	{
		"id" : 		3,
		"name" : 	"John Smith"
	},
];
var providers = [
	{
		"id" :		 1,
		"name" : 	 "Jim's Mowing", 
		"type" : 	 "Business", 
		"services" : "Lawnmowing and whipper snippering",
		"contact" : {
			"email" : 	"email@somewhere.com",
			"phone" : 	0402123456,
			"url" : 	"http://www.website.com"
		}
	},
	{
		"id" :		 2,
		"name" : 	 "Samantha Renshaw",
		"type" : 	 "Business",
		"services" : "House and office cleaning",
		"contact" : {
			"email" : 	"email@somewhere.com",
			"phone" : 	0402123456,
			"url" : 	"http://www.website.com"
		}
	},
	{
		"id" :		 3,
		"name" : 	 "St Vincent's",
		"type" : 	 "Charity",
		"services" : "Community work, counselling, outreach services",
		"contact" : {
			"email" : 	"email@somewhere.com",
			"phone" : 	0402123456,
			"url" : 	"http://www.website.com"
		}
	},
	{
		"id" :		 4,
		"name" : 	 "Computer Club!",
		"type" : 	 "Social Group",
		"services" : "Regular gatherings for computer enthusiasts",
		"contact" : {
			"email" : 	"email@somewhere.com",
			"phone" : 	0402123456,
			"url" : 	"http://www.website.com"
		}
	},
];
var providerTypes = ['Business', 'Charity', 'Social Group'];

