$(document).ready(function(){
	var $body = $('body'),
		$main = $body.find('main'),
		$settingBtns = $main.find('.settings'),
		$message = $main.find('.message'),
		$nav = $body.find('nav');
	if($message.length > 0) {
		$message.delay(3500);
		setTimeout(function(){
			$message.html('<p>Now go find what to do...</p>').delay(2500).fadeOut(2500);
			clearMessage();
		}, 3500);
		
	}
	function saveMessage(message) {
		clearMessage(250);
		setTimeout(function() {
			$('body').prepend(
				$('<section>').addClass('message')
					.html('<p>'+message+'</p>')
					.delay(2500).fadeOut(3500)
			);
		}, 250);
		
	}
	function clearMessage(t) {
		// var undefined;
		if (t === undefined) {
			t=6001;
		}
		console.log(t);
		setTimeout(function(){
			$('.message').remove();
		}, t);
	}

	$nav.find('.login').bind('mouseover', function(){
		$(this).find('ul').slideDown(250);
	});
	$nav.find('.login').bind('mouseleave', function(){
		$(this).find('ul').slideUp(250);
	});
	/**
	 * Settings button event handler
	 */
	$settingBtns.bind('click', function(){
		var settings = $(this).parent(), // Get the button's parent
			fields = settings.children().find('td'),
			glyph = $(this).find('span'); // Find the glyphicon
		glyph.toggleClass('glyphicon-pencil');
		glyph.toggleClass('glyphicon-ok');
		$(this).toggleClass('success');
		toggleFields(fields, settings);	// Toggle between text and form fields
		$(this).focusout();
	});

	/**
	 * Calls the appropriate function to toggle text and form fields.
	 * @param  {jQuery} fields   The fields to toggle.
	 * @param  {jQuery} settings The settings area to target.
	 * @return {void}
	 */
	function toggleFields(fields, settings) {
		if (settings.hasClass('profile') || settings.hasClass('listings')) {
			profileFormFields(fields);
		} else {
			optionFormFields(fields);
		}
	}

	/**
	 * Toggles between text and select dropdowns for the subscription
	 * settings section
	 * @param  {jQuery} fields A collection containing the fields 
	 *                         you want to target.
	 * @return {void}
	 */
	function optionFormFields(fields) {
		var input = false;
		if (fields.children('select').length < 1) {
			fields.each(function(){
				if(! this.className) return;
				var self = $(this),
					txt = self.text(),
					list = getOptions(this.className);
					list = dropDown(list);
					list.val(txt);
				self.html(list);
			});
			input = true;
		} else {
			fields.each(function() {
				if(! this.className) return;
				var self = $(this);
				self.text(self.find(':selected').text());
			});
			saveMessage('Options were saved...');
		}
		if (input) {
			fields.find('select:first').eq(0).focus();
		}
	}
	function glyph(glyphName) {
		return $('<span>').addClass('glyphicon glyphicon-' + glyphName);
	}
	/**
	 * Toggles between text and input displays for profile
	 * and listings menu.
	 * @param  {jQuery} fields A collection containing the fields 
	 *                         you want to target.
	 * @return {void}        
	 */

	function profileFormFields(fields) {
		var input = false;
		fields.each(function(){
			if(this.className) {
				if($(this).children('input').length < 1) {
					inputField(this);
					input = true;
				} else {
					textField(this);;
				}
			}
		});
		if(input) {
			fields.find('input:first').eq(0).focus();
		} else {
			saveMessage('Settings were saved...');
		}
	}
	function textField(obj) {
		if(obj.className.length > 1) {
			var self = $(obj),
				txt = self.find('input').val();
			if( self.hasClass('title') ) { 
				self.width('30%');
			}
			if( self.hasClass('description') ) { 
				self.width('70%');
			}
			self.text(txt);
		}
	}
	function inputField(obj) {
		if(obj.className.length > 1) {
			var self = $(obj),
				txt = self.text();
			self.html(
				$('<input>')
					.attr('type', 'text')
					.val(txt)
			);	
			if( self.hasClass('title') || self.hasClass('description') ) { 
				self.width('100%');
			}
		}
	}

	/**
	 * Returns a jQuery object which contains a select dropdown list.
	 * @param  {array} arr 	An array of option value pairs.
	 * @return {jQuery}     A select dropdown list containing option values.
	 */
	function dropDown(arr) {
		var sel = $('<select>').appendTo('body');
		$(arr).each(function() {
		 sel.append($("<option>").attr('value',this.val).text(this.text));
		});
		return sel;
	}


	/**
	 * Returns a set of option values according to className
	 * @param  {string} className The class of the target element.
	 * @return {array}            An array of option values.
	 */
	function getOptions(className) {
		var list = [];
		switch(className) {
			case 'subscriptions':
				list = [
					{val : 'Basic', text: 'Basic'},
					{val : 'Personal', text: 'Personal'},
					{val : 'Professional', text: 'Professional'}
				];
				break;
			case 'visibility':
				list = [
					{val : 'Public', text: 'Public'},
					{val : 'Limited', text: 'Limited'},
					{val : 'Private', text: 'Private'}
				];
				break;
			case 'alerts':
				list = [
					{val : 'App, email and text', text: 'App, email and text'},
					{val : 'Email Only', text: 'Email Only'},
					{val : 'None', text: 'None'}
				];
				break;
		}
		return list;
	}
});